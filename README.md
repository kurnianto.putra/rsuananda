### SOAL FRONTEND RSU ANANDA PURWOKERTO

### Installation

How to run this code on your local computer.?

```sh
$ git clone [linkrepo]
$ cd to directory
$ npm install
$ npm start
```

Sewaktu menjalankan koding terkena cors, ganti port yang berada pada `server.js baris ke 8` menjadi

```sh
var corsOptions = {
  origin: "http://localhost:3000",
};
```

### Plugins [Third Party]

| Plugin           | README                             |
| ---------------- | ---------------------------------- |
| Styled Component | https://styled-components.com/docs |

### Beberapa Catatan Penting Lainnya

Ketika saya meng-clone link gitlab untuk API Backend terdapat beberapa perubahan yang sata lakukan diantaranya :

- Saat pertama kali saya menjalankan server.js dan melakukan fecthing data ke route api terkendala `CORS` dan saya lakukan perubahan pada file `server.js` line ke `8` menjadi.

```sh
var corsOptions = {
origin: "http://localhost:3000",
};
```

- Saran , agar penamaan field disamakan saja baik itu route/body

```sh
  Contoh :
  Pada route : icd_nine_types
  Pada Body : icd_nines_type
```

- Mohon maaf sebelumnya, query pada route get-all yaitu `page` dan `size` sedikit membingungkan jika dibuatkan pagination. Asumsi saya page === limit dan size === offset. Untuk `page` tidak ada masalah, karena ketika data saya ada 15 dan dan query `page` saya beri nilai `5` maka data yang muncul hanya `5`. Namun untuk query `size` jika saya beri nilai 0 maka tidak ada masalah, namun ketika saya beri nilai `1` data tidak bisa muncul sama sekali `[]`. `*Bug Terlampir*`.
  Setahu saya offset itu `dimulai dari baris ke ..`. Andai kata saya punya 100 data dan ketika saya memasukkan `limit=5` & `offset=0` maka data yang muncul yaitu `0 -5` dan ketika saya ubah `limit=5 & offset=5` maka data yang muncul `5-10` dst..
  Dan lebih baik lagi penamaan offset dan limitnya tetap. Mohon maaf sebelumnya..
