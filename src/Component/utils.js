export function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = text && JSON.parse(text);
      return Promise.reject(error);
    }
    return data;
  });
}

let request = async (method, url, body) => {
  return await fetch(`${url}`, {
    method,
    body,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  }).then(handleResponse);
};

export const POST = (url, body) => {
  return request("POST", url, body);
};
export const GET = (url) => {
  return request("GET", url);
};

export const PUT = (url, body) => {
  return request("PUT", url, body);
};

export const DELETE = (url) => {
  return request("DELETE", url);
};
