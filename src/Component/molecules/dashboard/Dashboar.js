import React from "react";
import { FlexRow, FlexContainer } from "../../atoms/flexbox";
import { Judul } from "../../atoms/Body/style";

function DashboardICD({ jumlah }) {
  return (
    <FlexContainer padding={"5px 35px"} content={"flex-start"}>
      <FlexRow primary={"#2ee59d"} height={"auto"} size={1.5}>
        <Judul
          color={"#fff"}
          padding={"5px"}
          textAlign={"center"}
          fontSize={"20px"}
        >
          Jumlah ICD
        </Judul>
        <Judul textAlign={"center"} color={"#fff"} fontSize={"25px"}>
          {jumlah}
        </Judul>
      </FlexRow>
    </FlexContainer>
  );
}
export default DashboardICD;
