import { useState, useEffect } from "react";
import { FlexContainer, FlexRow } from "../../atoms/flexbox";
import FormICD from "./FormIcd";
import Button from "../../atoms/button";
import useICD from "../../hooks/useIcd";
import usePagination from "../../hooks/usePagination";
import Table from "../../atoms/tables";
import InputSearch from "../../atoms/search";
import DashboardICD from "../dashboard/Dashboar";
const ObjectForm = {
  id: "",
  icd_nines_name: "",
  icd_nines_code: "",
  icd_nines_type: "",
};

function ViewICD() {
  const [value, setValue] = useState(ObjectForm);
  const [search, setSearch] = useState("");
  const [counter, setCounter] = useState(0);
  const {
    _tambahICD,
    _hapusICD,
    loaderSimpan,
    _editData,
    data,
    setMode,
    mode,
    dataGabungan,
    newDocument,
  } = useICD();

  // Start Paging
  let itemsPerPage = 3;
  let startFrom = 1;
  const {
    slicedData,
    pagination,
    prevPage,
    nextPage,
    changePage,
    setFilteredData,
    setSearching,
  } = usePagination({ itemsPerPage, data, startFrom });
  let pages = Math.ceil(data.length / itemsPerPage);
  let currentPage = startFrom <= pages ? startFrom : 1;
  let newSliceData =
    slicedData.length === 0
      ? [...data].slice(
          (currentPage - 1) * itemsPerPage,
          currentPage * itemsPerPage
        )
      : slicedData;

  const ambilData = async (search) => {
    if (search.trim() !== "") {
      setSearching(true);
      const copiedData = [...data];
      const filtered = copiedData.filter((tindakan) => {
        let searchKey = "icd_nines_name";
        return tindakan[searchKey]
          .toLowerCase()
          .includes(search.trim().toLowerCase());
      });
      setFilteredData(filtered);
    } else {
      setFilteredData(data);
    }
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      ambilData(search);
    }
    return () => (isMounted = false);
  }, [counter, data]);

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      setValue(Object.assign({}, ObjectForm));
    }
    return () => (isMounted = false);
  }, [newDocument]);

  // Stop Paging

  function onChangeValue(e) {
    const { name, value } = e.target;
    setValue((prevState) => ({ ...prevState, [name]: value }));
  }

  return (
    <FlexContainer content={"center"} wrap={"wrap"}>
      <DashboardICD jumlah={dataGabungan.totalItems} />
      <FlexRow size={4} shadow>
        <FormICD
          value={value}
          onChangeValue={onChangeValue}
          mode={mode}
          simpanData={() => _tambahICD(value)}
          editData={() => _editData(value.id, value)}
          loader={loaderSimpan}
        />
      </FlexRow>
      <FlexRow size={7} height={"370px"}>
        <InputSearch
          placeholder={"Cari ICD Berdasarkan Nama"}
          value={search}
          onChange={(e) => {
            setSearch(e.target.value);
            setCounter(counter + 1);
          }}
        />
        <Table head={["Kode", "Nama", "Type", "Aksi"]}>
          <tbody>
            {slicedData.length > 0 ? (
              newSliceData.map((el, index) => (
                <tr key={index}>
                  <td>{el.icd_nines_code}</td>
                  <td>{el.icd_nines_name}</td>
                  <td>{el.icd_nines_type}</td>
                  <td>
                    <Button
                      bgColor={"rgba(238, 29, 29, 0.699)"}
                      text={"Hapus"}
                      padding={"10px"}
                      onClick={() => _hapusICD(el.id)}
                    />
                    <Button
                      text={"Edit"}
                      padding={"10px"}
                      onClick={() => {
                        setMode("edit");
                        setValue(el);
                      }}
                    />
                  </td>
                </tr>
              ))
            ) : (
              <tr>
                <td>Kata kunci {search} tidak ada di database</td>
              </tr>
            )}
          </tbody>
        </Table>
        <div className="containerpaging">
          <button className="paginationControls" onClick={prevPage}>
            &laquo;
          </button>
          <div className="pagination">
            <ul className="rowpaging">
              {pagination.map((page) => {
                if (!page.ellipsis) {
                  return (
                    <li key={page.id}>
                      <a
                        href="/#"
                        className={page.current ? "active" : ""}
                        onClick={(e) => changePage(page.id, e)}
                      >
                        {page.id}
                      </a>
                    </li>
                  );
                } else {
                  return (
                    <li key={page.id}>
                      <span className="pagination-ellipsis">&hellip;</span>
                    </li>
                  );
                }
              })}
            </ul>
          </div>
          <button className="paginationControls" onClick={nextPage}>
            &raquo;
          </button>
        </div>
      </FlexRow>
    </FlexContainer>
  );
}

export default ViewICD;
