import React from "react";
import { WrapperInput, Label, Input } from "../../atoms/inputs/style";
import Button from "../../atoms/button";

function FormIcd({ value, onChangeValue, mode, simpanData, editData, loader }) {
  function simpan() {
    mode === "create" ? simpanData() : editData();
  }
  return (
    <>
      <WrapperInput flexd={"column"}>
        <Label>Kode ICD</Label>
        <Input
          height={"40px"}
          onChange={onChangeValue}
          placeholder="Inputkan Kode ICD"
          name="icd_nines_code"
          value={value.icd_nines_code}
        />
      </WrapperInput>
      <WrapperInput flexd={"column"}>
        <Label>Name ICD</Label>
        <Input
          height={"40px"}
          onChange={onChangeValue}
          placeholder="Inputkan Nama ICD "
          name="icd_nines_name"
          value={value.icd_nines_name}
        />
      </WrapperInput>
      <WrapperInput flexd={"column"}>
        <Label>Type</Label>
        <Input
          height={"40px"}
          onChange={onChangeValue}
          placeholder="Inputkan Type ICD"
          name="icd_nines_type"
          value={value.icd_nines_type}
        />
      </WrapperInput>
      <WrapperInput>
        <Button
          onClick={simpan}
          padding={"15px"}
          text={`${
            loader ? "Menyimpan.." : mode === "create" ? "Simpan" : "Update"
          }`}
          bgColor={"#fff"}
          loader={loader}
        />
        <Button padding={"15px"} text={"Batal"} />
      </WrapperInput>
    </>
  );
}
export default FormIcd;
