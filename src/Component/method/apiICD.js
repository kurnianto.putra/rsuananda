import { POST, PUT, DELETE, GET } from "../utils";

const url = "http://localhost:8080/api/icd_nine_types";

export const tambahICD = (data) => POST(url, data);
export const editICD = (id, data) => PUT(`${url}/${id}`, data);
export const hapusICD = (id) => DELETE(`${url}/${id}`);
export const listICD = () => GET(`${url}?size=100`);
