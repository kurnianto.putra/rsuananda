import React from "react";
import { Wrapper, Judul } from "../atoms/Body/style";
import ViewICD from "../molecules/form";
import DashboardICD from "../molecules/dashboard/Dashboar";
function PageICD() {
  return (
    <Wrapper>
      <Judul fontSize={"20pt"} padding={"10px 45px"}>
        Kelola Tindakan ICD
      </Judul>
      <ViewICD />
    </Wrapper>
  );
}

export default PageICD;
