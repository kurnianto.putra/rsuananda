import { useEffect, useState } from "react";
import { tambahICD, listICD, hapusICD, editICD } from "../method/apiICD";
function useICD() {
  const [dataGabungan, setDataGabungan] = useState([]);
  const [data, setData] = useState([]);
  const [counter, setCounter] = useState(0);
  const [newDocument, setNewDocument] = useState(0);
  const [loaderSimpan, setLoader] = useState(false);
  const [mode, setMode] = useState("create");
  async function getData() {
    let api = await listICD();
    let respon = await api;
    setDataGabungan(respon);
    setData(respon.icd_nine_types);
  }

  useEffect(() => {
    let isMount = true;

    if (isMount) {
      getData();
    }

    return () => (isMount = false);
  }, [counter]);

  function _tambahICD(value) {
    setLoader(true);
    const formBody = Object.keys(value)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(value[key])
      )
      .join("&");
    tambahICD(formBody)
      .then(() => {
        alert(`Berhasil Simpan Data ${value.icd_nines_name}`);
        setCounter(counter + 1);
      })
      .catch((error) => console.error("Gagal", error))
      .finally(() => {
        setLoader(false);
        setNewDocument(newDocument + 1);
      });
  }

  async function _hapusICD(id) {
    await hapusICD(id)
      .then(() => {
        alert(`Berhasil hapus icd dengan id ${id}`);
        setCounter(counter + 1);
      })
      .catch((err) => console.error("gagal", err));
  }

  async function _editData(id, value) {
    setLoader(true);
    let obj = {
      id: id,
      icd_nines_name: value.icd_nines_name,
      icd_nines_code: value.icd_nines_code,
      icd_nines_type: value.icd_nines_type,
    };
    const formBody = Object.keys(obj)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(value[key])
      )
      .join("&");
    await editICD(id, formBody)
      .then(() => {
        alert(
          `Berhasil hapus data dengan id ${id} dan nama = ${value.icd_nines_name}`
        );
        setCounter(counter + 1);
      })
      .catch((Err) => console.error("Gagal", Err))
      .finally(() => {
        setLoader(false);
        setMode("create");
        setNewDocument(newDocument + 1);
      });
  }

  return {
    _tambahICD,
    _hapusICD,
    _editData,
    dataGabungan,
    loaderSimpan,
    data,
    setMode,
    mode,
    counter,
    newDocument,
  };
}

export default useICD;
