import styled from "styled-components";

export const FlexContainer = styled.div`
  background: ${(p) => p.background};
  flex-direction: ${(p) => p.flexDir};
  width: 100%;
  display: flex;
  height: ${(p) => p.height && "auto"};
  flex-wrap: ${(p) => p.wrap};
  justify-content: ${(p) => p.content};
  align-items: ${(p) => p.alignItem};
  padding: ${(p) => p.padding || "10px"};
  box-shadow: ${(p) => p.shadow && "10px 10px 25px -30px #080c21"};
`;
export const FlexRow = styled.div`
  width: ${(p) => (p.size / 12) * 100}vw;
  height: ${(p) => p.height};
  background-color: ${(p) => p.primary || "#fff"};
  margin: 5px;
  box-shadow: ${(p) => p.shadow && "10px 15px 25px -30px #080c21"};
  border-radius: 5px;
`;
