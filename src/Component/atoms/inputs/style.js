import styled from "styled-components";

export const WrapperInput = styled.div`
  margin: 4px;
  margin-top: 5px;
  padding: ${(p) => p.padding || "5px"};
  background: ${(p) => p.warna || "#fff"};
  width: ${(props) => props.panjang || "250px"};
  display: flex;
  flex-direction: ${(props) => props.flexd};
`;

export const Label = styled.label`
  color: #2a474d;
  font-weight: ${(p) => p.weight || "600"};
  display: block;
  font-size: 14px;
  margin-top: ${(p) => p.mt || "10px"};
  margin-bottom: 5px;
`;

export const Input = styled.input`
  width: auto;
  font-size: 15px;
  height: ${(props) => props.height || "auto"};
  border-radius: 2px;
  background: white;
  border: none;
  padding: 5px;
  color: #2a474d;
  border-bottom: 1px solid #22b47a;
  &::placeholder {
    color: #b0bec5;
  }
  &:focus {
    background: #f0f0f0;
    border-top: none;
    border-bottom: 2px solid #2ee59d;
  }
`;
