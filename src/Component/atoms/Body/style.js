import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: nowrap;
  flex-direction: column;
  width: 100%;
  margin-top: 10px;
  overflow: hidden;
`;
export const Judul = styled.p`
  font-weight: 600;
  font-size: ${(props) => props.fontSize};
  padding: ${(p) => p.padding};
  color: ${(p) => p.color || "#000"};
  text-align: ${(p) => p.textAlign};
`;
