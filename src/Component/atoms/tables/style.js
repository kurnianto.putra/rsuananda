import styled from "styled-components";
export const WrapperTable = styled.div`
  width: 100%;
  margin: auto;
  padding: 10px;
  padding-bottom: 30px;
  z-index: 1;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;

export const StyleTable = styled.table`
  box-shadow: 4px 4px 10px rgba(190, 191, 192, 0.33);
  border-spacing: 1px;
  background: transparent;
  border-collapse: collapse;
  background: #fff;
  border-radius: 10px;
  z-index: 1;
  text-transform: capitalize;
  width: 100%;
  margin-left: ${(p) => p.ml};
  position: relative;

  td,
  th {
    padding-left: 8px;
  }
  thead > tr {
    height: 50px;
    background: #2ee59d;
  }
  tbody tr {
    height: 60px;

    &:last-child {
      border: 0;
    }
  }
  td,
  th {
    text-align: center;
  }

  th {
    font-size: 18px;
    color: #fff;
    line-height: 1.2;
    font-weight: unset;
  }

  tr {
    &:nth-child(even) {
      background-color: rgb(225, 232, 236);
      color: #0099ff;
      border-left: 5px solid #1c84e5;
    }
    font-size: 15px;
    color: black;
    line-height: 1.2;
    font-weight: unset;
  }

  tbody tr:hover {
    color: #555555;
    background-color: #f5f5f5;
  }
`;
