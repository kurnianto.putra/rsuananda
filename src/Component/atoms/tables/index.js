import { WrapperTable, StyleTable } from "./style";

function Table({ head, children }) {
  return (
    <WrapperTable>
      <StyleTable>
        <thead>
          <tr>{head && head.map((el, index) => <th key={index}>{el}</th>)}</tr>
        </thead>
        {children}
      </StyleTable>
    </WrapperTable>
  );
}
export default Table;
