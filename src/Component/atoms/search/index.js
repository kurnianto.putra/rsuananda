import { SearchInput, Icon, SearchWrapper } from "./style";
function InputSearch({ placeholder, name, value, onChange }) {
  return (
    <SearchWrapper>
      <Icon content={"\f002"}>
        <SearchInput
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={onChange}
        />
      </Icon>
    </SearchWrapper>
  );
}

export default InputSearch;
