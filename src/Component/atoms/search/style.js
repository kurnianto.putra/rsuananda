import styled from "styled-components";

export const SearchWrapper = styled.div`
  position: relative;
  flex-direction: row;
  justify-content: space-around;
  width: ${(p) => p.lebar || "100%"};
  margin-left: ${(p) => p.ml};
  align-items: center;
  padding: 5px;
  z-index: 3;
`;
export const Icon = styled.div`
  width: auto;
  color: #2ee59d;

  &:before {
    content: "${(props) => props.content}";
    font-family: "Font Awesome\ 5 Free";
    font-weight: 900;
    text-decoration: inherit;
    font-size: 20px;
    position: absolute;
    left: 0;
    margin-left: 10px;
    line-height: 2;
  }
`;
export const SearchInput = styled.input`
  padding: 4px;
  background: #f9f8f8;
  border: 1px solid white;
  box-shadow: 0px 4px 4px rgba(196, 196, 196, 0.25);
  border-radius: 5px;
  width: 100%;
  padding-left: 30px;
  height: 40px;
  font-size: 12pt;
  transition: 0.3s;

  &:focus {
    border: 0px;
    border-bottom: 2px solid #66dcec;
    animation: all 3.5s;
    border-radius: 3px;
    font-weight: 500;
  }

  &:before {
  }
`;
