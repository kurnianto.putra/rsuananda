import { StyledButton } from "./style";

function Button({ onClick, children, loader, text, ...rest }) {
  return (
    <StyledButton
      onClick={(e) => window.confirm("Apakah anda yakin.?") && onClick(e)}
      disabled={loader}
      {...rest}
    >
      {loader ? (
        <span>
          <i className="fas fa-spinner fa-spin"></i> Menyimpan...
        </span>
      ) : (
        text
      )}
    </StyledButton>
  );
}
export default Button;
