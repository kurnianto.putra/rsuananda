import styled from "styled-components";

export const StyledButton = styled.button`
  font-size: 10pt;
  border-radius: 5px;
  padding: ${(p) => p.padding};
  margin: 10px;
  cursor: ${(p) => (p.cursor ? "not-allowed" : "pointer")};
  width: auto;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-weight: 500;
  color: #000;
  background-color: ${(p) => p.bgColor};
  border: none;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s;
  outline: none;
  &:hover {
    background-color: #2ee59d;
    box-shadow: 0px 15px 20px rgba(46, 229, 157, 0.4);
    color: #fff;
    transform: translateY(-2px);
  }
`;
